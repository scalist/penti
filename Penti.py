import gi

gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')

from gi.repository import Gtk, WebKit2

class Browser(Gtk.Window):
    def __init__(self, app_name, url, x, y):
        super(Browser, self).__init__()

        # create window obj
        self.win = Gtk.Window()

        # basics
        self.win.set_title(app_name)
        self.win.set_default_size(x, y)
        self.win.connect('destroy', Gtk.main_quit)
        
        # webview
        self.webview = WebKit2.WebView()
        if url.startswith("http") or url.startswith("file"):
            self.webview.load_uri(url)
        else:
            self.webview.load_uri(f'http://{url}/') # default loading url
        self.win.add(self.webview)

        # display
        self.win.show_all()
        Gtk.main()

# new instance api
def Create(app_name='Penti', url='scalist.net', x=800, y=400): # NOTE it automatically concats http://
    print(f'Creating penti browser instance with app name of \'{app_name}\'.')
    browser_inst = Browser(app_name, url, x, y)
    print(f'Deleted penti browser instance with app name of \'{app_name}\'.')
